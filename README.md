# UI



## Getting Started



### Prerequisites

Install Node.js.

### Clone 

git clone https://manojpandeyait@bitbucket.org/manojpandeyait/wipfliui.git

### Install Dependencies


go to command promt and run below command

```
npm install
```

Behind the scenes this will also call `bower install`.  You should find that you have two new
folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `app/bower_components` - contains the angular framework files

### Run the Application

Copy the app folder and copy it to {tomcat_home}/webapps folder
