(function () {
    'use strict';

	angular.module('SalesTrackerApp', ['ui.bootstrap', 'ngAnimate', 'ngSanitize', 'ngRoute', 'ngResource', 'ngCookies']);

})();