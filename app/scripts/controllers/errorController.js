/**
 * Created by asheesh on 21/2/14.
 */

'use strict';

angular.module("SalesTrackerApp").controller('errorController',
    ['$scope', '$routeParams', '$rootScope', '$timeout',
        function ($scope, $routeParams, $rootScope, $timeout) {
            $scope.errorCode = $routeParams.errorCode;
            if ($scope.errorCode == 404 || $scope.errorCode == 418) {
                $scope.errorMsg = $routeParams.message ? $routeParams.message : "The page you requested cannot be found.";
                $scope.errorDesc = "Maybe the page you requested was moved or deleted, or perhaps you just mistyped \n"
                    + "the address.\n\n"
                    + "Why not try and find your way using navigation bar above or click on the logo to "
                    + "return to home page";
            } else if ($scope.errorCode == 500) {
                $scope.errorMsg = $routeParams.message ? $routeParams.message : "Some error occurred";
                $scope.errorDesc = "Please start again. If you are getting this error again,\n"
                    + "Please report the admin with proper description of the error and steps to reproduce.\n\n"
                    + "Thank you for you kind support.";
            } else if ($scope.errorCode == 403) {
                $scope.errorMsg = $routeParams.message ? $routeParams.message : "Unauthorized: Access is denied";
                $scope.errorDesc = "You do not have permission to view this directory or page using the credentials that you supplied.\n\n"
                    + "Contact administrator to access this page.\n\n"
                    + "Thank you for you kind support.";
            }

            $timeout(function () {
                window.scrollTo(0, 0);
            }, 0);
        }])
;