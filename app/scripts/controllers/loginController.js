/**
 * Created by asheesh on 21/2/14.
 */

'use strict';

angular.module("SalesTrackerApp").controller('loginController',
    ['$scope', '$routeParams', 'salesLeadService','$location','$cookieStore',
        function ($scope, $routeParams, salesLeadService, $location,$cookieStore) {
            $scope.hideUserHeader();
            $scope.loginKey = $routeParams.loginKey;
            $scope.showLoading();
            salesLeadService.getUserByKey($scope.loginKey).then(
                function(user){
                    $scope.hideLoading();
                    if(user){
                        $cookieStore.put("logged-user",user);
                        $scope.loggedUser = user;

                    }else{
                        $location.path("/error/403").search({});
                    }
                },
                function(){
                    $scope.hideLoading();
                    $location.path("/error/500").search({});
                }
            );
            $scope.proceed = function(){
                $location.path("/welcome").search({});
            }
        }]);