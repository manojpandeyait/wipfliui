/**
 * Created by asheesh on 21/2/14.
 */

'use strict';

angular.module("SalesTrackerApp").controller('adminController',
    ['$scope', '$routeParams', 'salesLeadService','$location','$timeout',
        function ($scope, $routeParams, salesLeadService, $location,$timeout) {
            $scope.loggedUser = salesLeadService.getLoggedUser();
            $scope.showUserHeader($scope.loggedUser);

            if(!$scope.loggedUser || ($scope.loggedUser && $scope.loggedUser.role != "ROLE_ADMIN")){
                $location.path("/error/403").search({});
                return;
            }

            $scope.showLoading();
            salesLeadService.getUsers().then(
                function(data){
                    $scope.users = data;
                    $scope.hideLoading();
                },
                function(){
                    $scope.hideLoading();
                    $location.path("/error/500").search({});
                    return;
                }
            );

            $scope.invitationSubject = "Wipfli Partner Summit";
            $scope.invitationDescription = "Hi {username},\n\n" +
                "You are receiving this email as part of Wipfli Partner Summit.\n\n" +
                "Link to access the application is:\n{appurl}\n\n" +
                "Thanks,\n" +
                "Wipfli.";

            // selected fruits
            $scope.selectedUsers = [];

            // toggle selection for a given fruit by name
            $scope.toggleSelection = function toggleSelection(userId) {
                var idx = $scope.selectedUsers.indexOf(userId);

                if (idx > -1) {
                    $scope.selectedUsers.splice(idx, 1);
                }
                else {
                    $scope.selectedUsers.push(userId);
                }
                if($scope.selectedUsers.length !== $scope.users.length){
                    $scope.isAllUserChecked = "false";
                }else{
                    $scope.isAllUserChecked = "true";
                }
            };

            $scope.checkAllUser = function(){
                $timeout(function(){
                    $scope.selectedUsers = [];
                    if($scope.isAllUserChecked == "true"){
                        angular.forEach($scope.users,function(user){
                            $scope.selectedUsers.push(user.id);
                        });
                    }
                },0);
            };
            $scope.validateAndSendInvite = function(){
                $scope.showLoading();
                $scope.btnDisabled = true;
                if($scope.validate()){
                    salesLeadService.sendInvitation($scope.selectedUsers,$scope.invitationSubject,$scope.invitationDescription).then(
                        function(data){
                            $scope.btnDisabled = false;
                            $scope.hideLoading();
                            $scope.showToastrSuccess("invitation sent");
                        },
                        function(){
                            $scope.showToastrErrors("could not send invitation");
                            $scope.btnDisabled = false;
                            $scope.hideLoading();
                        }
                    );
                }else{
                    $scope.hideLoading();
                    $scope.btnDisabled = false;
                }
            };
            $scope.validate = function(){
                var isValid = true;
                $scope.resetErrors();

                if (!$scope.invitationSubject || $scope.invitationSubject.trim() == "") {
                    $scope.invitationSubjectError = "Invitation subject is required";
                    isValid = false;
                }
                if(!$scope.invitationDescription || $scope.invitationDescription.trim() == ""){
                    $scope.invitationDescriptionError = "Invitation body is required";
                    isValid = false;
                }
                return isValid;
            };
            $scope.resetErrors = function(){
                $scope.invitationSubjectError = "";
                $scope.invitationDescriptionError = "";
            };
            $timeout(function () {
                $scope.setTextAreaHeight(angular.element(".invitation-subject")[0]);
            }, 0);

        }]);