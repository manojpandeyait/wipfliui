(function() {
	'use strict';

	angular.module('SalesTrackerApp').controller(
			'ClientController',
			[ '$scope','salesLeadService','$location','$timeout','SEARCH_TIMEOUT', clientController ]);

	function clientController( $scope, salesLeadService,$location,$timeout,SEARCH_TIMEOUT) {
        $scope.loggedUser = salesLeadService.getLoggedUser();
        $scope.showUserHeader($scope.loggedUser);
        $scope.targetClientCurrentScreen = 1;
        $scope.planOfActions =[];
        $scope.clientPlanOfActions =[];
        $scope.collaborativeUsers =[];
        $scope.targetClientCommonInterests =[];
        $scope.readMode = false;
        $scope.isClientSocialized = false;
        $scope.clientSearchPromise = null;
        $scope.userSearchPromise = null;
        $scope.planOfActionInEditMode = null;
        $scope.isPlanOfActionInEditMode = false;
        $scope.planOfActionToUpdate = '';
        $scope.planOfActionToUpdateError = '';

        if(!$scope.loggedUser){
            $location.path("/error/403").search({});
            return;
        }

        var loadTargetClientInfo = function(loggedUser) {
            $scope.showLoading();
            salesLeadService.getSocializedTargetClient(loggedUser).then(
                function (data) {
                    if (data) {
                        $scope.isClientSocialized = true;
                        $scope.selectedClient = data.targetClient.client;
                        $scope.selectedClientName = $scope.selectedClient.name;

                        $scope.targetClient = data.targetClient;
                        $scope.currentAnnualRevenue = $scope.targetClient.currentAnnualRevenue;
                        $scope.potentialAnnualRevenue = $scope.targetClient.potentialAnnualRevenue;

                        $scope.clientPlanOfActions = [];
                        $scope.planOfActions = [];
                        angular.forEach($scope.targetClient.actionPlans, function (planOfAction) {
                            $scope.clientPlanOfActions.push(planOfAction.description);
                            $scope.planOfActions.push(planOfAction);
                        });
                        $scope.collaborativeUsers = data.targetClient.helpers;
                        $scope.targetClientCommonInterests = data.targetClientCommonInterest;
                        $scope.btnDisabled = false;
                        $scope.readMode = true;
                        $scope.targetClientCurrentScreen = 1;
                        $scope.hideLoading();
                    } else {
                        $scope.btnDisabled = false;
                        $scope.hideLoading();
                        $scope.readMode = false;
                    }
                },
                function () {
                    $scope.btnDisabled = false;
                    $scope.hideLoading();
                    $location.path("/error/500").search({});
                    return;
                }
            );
        };
        loadTargetClientInfo($scope.loggedUser);
        angular.element("body").off("click",".target-client-tab")
            .on("click",".target-client-tab", function(){
                loadTargetClientInfo($scope.loggedUser);
            });
        $scope.clientSearchPromise = null;
        $scope.searchClients = function(searchCriteria){
            if(searchCriteria) {
                if($scope.clientSearchPromise){
                    $scope.hideLoading();
                    $timeout.cancel($scope.clientSearchPromise);
                }
                if(searchCriteria.trim().length>2) {
                    $scope.showLoading();
                    $scope.clientSearchPromise =  $timeout(function(){
                        return salesLeadService.searchClients(searchCriteria).then(function (data) {
                            $scope.hideLoading();
                            return data;
                        }, function () {
                            $scope.hideLoading();
                            $location.path("/error/500").search({});
                            return;
                        });
                    },SEARCH_TIMEOUT);
                    return $scope.clientSearchPromise;
                }else{
                    return [];
                }
            }
        };
        $scope.clientSelected = function(selectedClient){
            $scope.showLoading();
            salesLeadService.getClientById(selectedClient).then(function (client) {
                $scope.selectedClient = client;
                $scope.selectedClientName = client.name;
                $scope.hideLoading();
            }, function () {
                $scope.hideLoading();
            });
        };

        $scope.searchUsers = function(searchCriteria){
            if(searchCriteria) {
                if($scope.userSearchPromise){
                    $scope.hideLoading();
                    $timeout.cancel($scope.userSearchPromise);
                }
                if(searchCriteria.trim().length>1) {
                    $scope.showLoading();
                    $scope.userSearchPromise =  $timeout(function(){
                        return salesLeadService.searchUsers(searchCriteria,$scope.loggedUser).then(function (data) {
                            $scope.hideLoading();
                            return data;
                        }, function () {
                            $scope.hideLoading();
                            $location.path("/error/500").search({});
                            return;
                        });
                    },SEARCH_TIMEOUT);
                    return $scope.userSearchPromise;
                }else{
                    return [];
                }
            }
        };
        $scope.collaborativeUserSelected = function(collaborativeUser){
            $scope.userToAdd = collaborativeUser;
        };

        $scope.submitToSocialize = function(){
            $scope.btnDisabled = true;
            $scope.showLoading();
            var targetClientDataToSocialize = {
                "id": null,
                "currentAnnualRevenue": $scope.currentAnnualRevenue,
                "potentialAnnualRevenue": $scope.potentialAnnualRevenue,
                "clientId": $scope.selectedClient.id,
                "userId": $scope.loggedUser.id,
                helpers:[],
                actionPlans:[]
            };
            angular.forEach($scope.clientPlanOfActions,function(planOfAction){
                var POA = {
                    "targetClientId": null,
                    "targetProspectId": null,
                    "description": planOfAction
                };
                targetClientDataToSocialize.actionPlans.push(POA);
            });
            salesLeadService.submitTargetClient(targetClientDataToSocialize).then(
                function(){
                    $scope.isClientSocialized = true;
                    salesLeadService.getSocializedTargetClient($scope.loggedUser).then(
                        function(data){
                            $scope.targetClient = data.targetClient;
                            $scope.collaborativeUsers = data.targetClient.helpers;
                            $scope.clientPlanOfActions = [];
                            $scope.planOfActions = [];
                            angular.forEach($scope.targetClient.actionPlans, function (planOfAction) {
                                $scope.clientPlanOfActions.push(planOfAction.description);
                                $scope.planOfActions.push(planOfAction);
                            });
                            $scope.targetClientCommonInterests = data.targetClientCommonInterest;
                            $scope.btnDisabled = false;
                            $scope.readMode = true;
                            $scope.targetClientCurrentScreen = 1;
                            $scope.hideLoading();
                            $scope.showToastrSuccess("target client socialized");
                        },
                        function(){
                            $scope.btnDisabled = false;
                            $scope.hideLoading();
                            $scope.showToastrErrors("could not socialize");
                        }
                    );
                },
                function(){
                    $scope.btnDisabled = false;
                    $scope.hideLoading();
                    $scope.showToastrErrors("could not socialize");
                }
            )
        };
        $scope.addCollaborativeUser = function(){
            $scope.showLoading();
            $scope.btnDisabled = true;
            if(validateScreen3()){
                salesLeadService.addCollaborativeUser($scope.userToAdd, $scope.targetClient,true).then(
                    function(data){
                        $scope.collaborativeUsers.push(data);
                        $scope.collaborativeUserName = "";
                        $scope.btnDisabled = false;
                        $scope.hideLoading();
                        $scope.showToastrSuccess("collaborative user added");
                    },
                    function(){
                        $scope.showToastrErrors("could not add");
                        $scope.btnDisabled = false;
                        $scope.hideLoading();
                    }
                );
            }else{
                $scope.btnDisabled = false;
                $scope.hideLoading();
            }
        };
        $scope.deleteCollaborativeUser = function(collaborativeUser){
            $scope.showLoading();
            $scope.btnDisabled = true;
            salesLeadService.deleteCollaborativeUser(collaborativeUser).then(
                function(data){
                    for(var i=0;i<$scope.collaborativeUsers.length;i++){
                        if($scope.collaborativeUsers[i].id == collaborativeUser.id){
                            $scope.collaborativeUsers.splice(i,1);
                        }
                    }
                    $scope.btnDisabled = false;
                    $scope.hideLoading();
                    $scope.showToastrSuccess("collaborative user deleted");
                },
                function(){
                    $scope.showToastrErrors("could not delete");
                    $scope.btnDisabled = false;
                    $scope.hideLoading();
                }
            );
        };

        var validateScreen1 = function () {
            var isValid = true;
            $scope.resetErrorsOnScreen1();

            if (!$scope.selectedClientName || !$scope.selectedClient || ($scope.selectedClientName !== $scope.selectedClient.name)) {
                $scope.clientError = "Client is required";
                isValid = false;
            }

            if ($scope.currentAnnualRevenue) {
                if (isNaN($scope.currentAnnualRevenue) || $scope.currentAnnualRevenue.toString().indexOf("-") >= 0
                    || $scope.currentAnnualRevenue.toString().indexOf("+") >= 0) {
                    $scope.currentAnnualRevenueError = "Current Annual Revenue should should be between 0 & 999999999";
                    isValid = false;
                } else if (!isNaN($scope.currentAnnualRevenue) && $scope.currentAnnualRevenue.toString().indexOf(".") >= 0) {
                    $scope.currentAnnualRevenueError = "Current Annual Revenue should should be between 0 & 999999999";
                    isValid = false;
                } else if (parseInt($scope.currentAnnualRevenue) < 0 ||  parseInt($scope.currentAnnualRevenue) > 999999999) {
                    $scope.currentAnnualRevenueError = "Current Annual Revenue should should be between 0 & 999999999";
                    isValid = false;
                }
            }

            if ($scope.potentialAnnualRevenue) {
                if (isNaN($scope.potentialAnnualRevenue) || $scope.potentialAnnualRevenue.toString().indexOf("-") >= 0
                    || $scope.potentialAnnualRevenue.toString().indexOf("+") >= 0) {
                    $scope.potentialAnnualRevenueError = "Potential Annual Revenue should should be between 0 & 999999999";
                    isValid = false;
                } else if (!isNaN($scope.potentialAnnualRevenue) && $scope.potentialAnnualRevenue.toString().indexOf(".") >= 0) {
                    $scope.potentialAnnualRevenueError = "Potential Annual Revenue should should be between 0 & 999999999";
                    isValid = false;
                } else if (parseInt($scope.potentialAnnualRevenue) < 0 ||  parseInt($scope.potentialAnnualRevenue) > 999999999) {
                    $scope.potentialAnnualRevenueError = "Potential Annual Revenue should should be between 0 & 999999999";
                    isValid = false;
                }
            }else{
                $scope.potentialAnnualRevenueError = "Potential Annual Revenue is required";
                isValid = false;
            }
            return isValid;
        };
        var validateAddPlanOfAction = function () {
            var isValid = true;
            $scope.planOfActionToAddError = "";

            if (!$scope.planOfActionToAdd || $scope.planOfActionToAdd.trim() == "") {
                $scope.planOfActionToAddError = "Plan of Action is required";
                isValid = false;
            }else if($scope.planOfActionToAdd  && $scope.planOfActionToAdd.trim().length > 100){
                $scope.planOfActionToAddError = "Plan of Action should be less than 100 characters";
                isValid = false;
            }else if($scope.clientPlanOfActions.indexOf($scope.planOfActionToAdd)>=0){
                $scope.planOfActionToAddError = "Duplicate Plan of Action";
                isValid = false;
            }
            return isValid;
        };
        var validateUpdatePlanOfAction = function (planOfActionToUpdate,currentPlanOfAction) {
            var isValid = true;
            $scope.planOfActionToUpdateError = "";

            if (!planOfActionToUpdate || planOfActionToUpdate.trim() == "") {
                $scope.planOfActionToUpdateError = "Plan of Action is required";
                isValid = false;
            }else if(planOfActionToUpdate  && planOfActionToUpdate.trim().length > 100){
                $scope.planOfActionToUpdateError = "Plan of Action should be less than 100 characters";
                isValid = false;
            }else if($scope.clientPlanOfActions.indexOf(planOfActionToUpdate)>=0){
                if(currentPlanOfAction != planOfActionToUpdate){
                    $scope.planOfActionToUpdateError = "Duplicate Plan of Action";
                    isValid = false;
                }
            }
            return isValid;
        };
        var validateScreen3 = function () {
            var isValid = true;
            $scope.resetErrorsOnScreen3();

            if (!$scope.collaborativeUserName || $scope.collaborativeUserName.trim() == "" || !$scope.userToAdd) {
                $scope.collaborativeUserToAddError = "collaborative user is required";
                isValid = false;
            }else if($scope.collaborativeUserName && $scope.userToAdd && $scope.collaborativeUserName != $scope.userToAdd.username) {
                $scope.collaborativeUserToAddError = "collaborative user is required";
                isValid = false;
            }else{
                var alreadyAdded = false;
                angular.forEach($scope.collaborativeUsers, function(collaborativeUser){
                    if(collaborativeUser.user.id == $scope.userToAdd.id){
                        alreadyAdded = true;
                    }
                });
                if(alreadyAdded){
                    $scope.collaborativeUserToAddError = "user already added";
                    isValid = false;
                }
            }
            return isValid;
        };

        $scope.validateScreenGoToNext = function(){
            if($scope.targetClientCurrentScreen == 1){
                if(validateScreen1()){
                    $scope.targetClientCurrentScreen++;
                }
            }else if($scope.targetClientCurrentScreen == 2){
                $scope.targetClientCurrentScreen++;
            }
        };

        $scope.openPlanOfActionToEdit = function($index,planOfAction){
            $scope.planOfActionInEditMode = $index;
            $scope.isPlanOfActionInEditMode = true;
            $scope.planOfActionToUpdate = planOfAction;
            $scope.planOfActionToUpdateError='';
        };
        $scope.cancelPlanOfActionEdit = function(){
            $scope.planOfActionInEditMode = null;
            $scope.isPlanOfActionInEditMode = false;
            $scope.planOfActionToUpdate='';
            $scope.planOfActionToUpdateError='';
        };
        $scope.validateAndAddPlanOfActionToClient = function(){
            $scope.showLoading();
            if(validateAddPlanOfAction()){
                $scope.clientPlanOfActions.push($scope.planOfActionToAdd);
                $scope.planOfActionToAdd = "";
                $scope.hideLoading();
            }else{
                $scope.hideLoading();
            }
        };
        $scope.validateAndAddPlanOfAction = function(){
            $scope.showLoading();
            if(validateAddPlanOfAction()){
                $scope.btnDisabled = true;
                salesLeadService.addPlanOfAction($scope.planOfActionToAdd, $scope.targetClient,true).then(
                    function(data){
                        $scope.planOfActions.push(data);
                        $scope.clientPlanOfActions.push(data.description);
                        $scope.planOfActionToAdd = "";
                        $scope.btnDisabled = false;
                        $scope.hideLoading();
                        $scope.showToastrSuccess("plan of action added");
                    },
                    function(){
                        $scope.showToastrErrors("could not add");
                        $scope.btnDisabled = false;
                        $scope.hideLoading();
                    }
                );
            }else{
                $scope.hideLoading();
            }
        };
        $scope.validateAndUpdateClientPlanOfAction = function(planOfActionToUpdate,currentPlanOfAction){
            $timeout(function(){
                $scope.showLoading();
                if(validateUpdatePlanOfAction(planOfActionToUpdate,currentPlanOfAction)){
                    angular.forEach($scope.clientPlanOfActions,function(planOfAction,index){
                        if(currentPlanOfAction == planOfAction){
                            $scope.clientPlanOfActions[index] = planOfActionToUpdate;
                        }
                    });
                    $scope.planOfActionToUpdate = "";
                    $scope.isPlanOfActionInEditMode = false;
                    $scope.planOfActionInEditMode = null;
                    $scope.hideLoading();
                }else{
                    $scope.hideLoading();
                }
            });

        };
        $scope.validateAndUpdatePlanOfAction = function(planOfActionToUpdate,currentPlanOfAction){
            $scope.showLoading();
            if(validateUpdatePlanOfAction(planOfActionToUpdate,currentPlanOfAction.description)){
                $scope.btnDisabled = true;
                salesLeadService.updatePlanOfAction({id:currentPlanOfAction.id,description:planOfActionToUpdate}).then(
                    function(data){
                        angular.forEach($scope.planOfActions,function(planOfAction,index){
                            if(currentPlanOfAction.id == planOfAction.id){
                                $scope.planOfActions[index] = data;
                            }
                        });
                        angular.forEach($scope.clientPlanOfActions,function(planOfAction,index){
                            if(currentPlanOfAction.description == planOfAction){
                                $scope.clientPlanOfActions[index] = data.description;
                            }
                        });
                        $scope.planOfActionToUpdate = "";
                        $scope.isPlanOfActionInEditMode = false;
                        $scope.planOfActionInEditMode = null;
                        $scope.btnDisabled = false;
                        $scope.hideLoading();
                        $scope.showToastrSuccess("plan of action updated");
                    },
                    function(){
                        $scope.showToastrErrors("could not add");
                        $scope.btnDisabled = false;
                        $scope.hideLoading();
                    }
                );
            }else{
                $scope.hideLoading();
            }
        };
        $scope.deletePlanOfActionFromClient = function(planOfAction){
            $scope.showLoading();
            $scope.clientPlanOfActions.splice($scope.clientPlanOfActions.indexOf(planOfAction),1);
            $scope.hideLoading();
        };
        $scope.deletePlanOfAction = function(planOfAction){
            $scope.showLoading();
            $scope.btnDisabled = true;
            salesLeadService.deletePlanOfAction(planOfAction).then(
                function(data){
                    for(var i=0;i<$scope.planOfActions.length;i++){
                        if($scope.planOfActions[i].id == planOfAction.id){
                            $scope.planOfActions.splice(i,1);
                        }
                    }
                    $scope.clientPlanOfActions.splice($scope.clientPlanOfActions.indexOf(planOfAction.description),1);
                    $scope.btnDisabled = false;
                    $scope.hideLoading();
                    $scope.showToastrSuccess("plan of action deleted");
                },
                function(){
                    $scope.showToastrErrors("could not delete");
                    $scope.btnDisabled = false;
                    $scope.hideLoading();
                }
            );
        };

        $scope.resetErrorsOnScreen1 = function () {
            $scope.clientError = "";
            $scope.currentAnnualRevenueError = "";
            $scope.potentialAnnualRevenueError = "";
        };
        $scope.resetErrorsOnScreen3 = function () {
            $scope.collaborativeUserToAddError = "";
        };

	};
})();
