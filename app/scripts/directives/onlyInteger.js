/**
 * Created by asheesh on 16/04/2014.
 */
angular.module('SalesTrackerApp').directive('onlyInteger', [ function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attr, ngModel) {
            var numberPattern = attr.decimal == "0" ? /[\d]+/g : /[\d.]+/g;
            var applyFormatting = function (event) {
                if (attr.formatter) {
                    var prefix = "", suffix = "", separator = ",";
                    if (attr.formatter == "premium") {
                        prefix = "$ ";
                        suffix = "";
                    } else if (attr.formatter == "ratio") {
                        prefix = "";
                        suffix = " %";
                    } else if (attr.formatter == "count") {
                        prefix = "";
                        suffix = "";
                    } else if (attr.formatter == "decimalWithoutSeparator") {
                        prefix = "";
                        suffix = "";
                        separator = "";
                    }

                    var number = event.currentTarget.value.match(numberPattern) ? event.currentTarget.value.match(numberPattern).join("") : '';

                    var formattedValue = scope.maskNumbers(number, 0, prefix, suffix, separator);
                    ngModel.$setViewValue(formattedValue);
                    ngModel.$render();
                }
            };
            element.on('keydown', function (event) {
                if (!(event.ctrlKey || event.altKey || event.shiftKey)) {
                    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                        (event.keyCode >= 96 && event.keyCode <= 105)) {
                        if (event.currentTarget.value.indexOf(".") >= 0) {
                            if ((parseInt(event.currentTarget.value.split(".")[1]) | 0) !== 0) {
                                if (parseInt(event.currentTarget.value.split(".")[1]).toString().length <= (parseInt(attr.decimal) - 1)) {
                                    return true;
                                } else {
                                    if (event.target.selectionStart > event.currentTarget.value.indexOf(".")) {
                                        return false;
                                    } else {
                                        return true;
                                    }
                                }
                            } else if (event.target.selectionStart <= (event.currentTarget.value.indexOf("."))) {
                                return true;
                            } else if ((parseInt(event.currentTarget.value.split(".")[1]) | 0) === 0) {
                                return true;
                            }
                        } else if (event.currentTarget.value.indexOf("-") >= 0 && event.target.selectionStart == 0) {
                            return false;
                        } else {
                            return true;
                        }

                    } else if (event.keyCode == 8 || event.keyCode == 13 ||
                        event.keyCode == 9 || event.keyCode == 46 ||
                        event.keyCode == 35 || event.keyCode == 36 ||
                        event.keyCode == 37 || event.keyCode == 39) {
                        return true;
                    }
                    else if (event.keyCode == 109 || event.keyCode == 189) {
                        if (event.currentTarget.value.indexOf("-") < 0 && (event.target.selectionStart == 0 && attr.positive != "true")) {
                            return true;
                        }
                    } else if (event.currentTarget.value.indexOf(".") < 0 && (event.keyCode == 110 || event.keyCode == 190)) {
                        return !(attr.decimal == "0");
                    }
                } else {
                    if (event.keyCode == 67 || event.keyCode == 86) {
                        return true;
                    }
                }
                return  (event.shiftKey && event.keyCode == 9);
            });
            element.on('blur', function (event) {
                applyFormatting(event);
                element.on('keyup', function (event) {
                    if (attr.formatter) {
                        if (ngModel.$parsers.length == 0) {
                            ngModel.$parsers.push(function (value) {
                                var number = value.match(numberPattern) ? value.match(numberPattern).join("") : '';
                                return number;
                            });
                        }
                    }
                });
            });
            element.on('keyup', function (event) {
                if (event.ctrlKey && event.keyCode == 86) {
                    applyFormatting(event);
                }
            });
            element.on('change', function (event) {
                if (attr.formatter) {
                    if (ngModel.$parsers.length == 0) {
                        ngModel.$parsers.push(function (value) {
                            var number = value.match(numberPattern) ? value.match(numberPattern).join("") : '';
                            return number;
                        });
                    }
                }
            });
        }
    }
}]);