/**
 * Created by asheesh on 21/04/2014.
 */
'use strict';

angular.module('SalesTrackerApp').directive('confirmClick', ['$modal', '$rootScope', function ($modal, $rootScope) {
    return {
        priority: 1,
        terminal: true,
        link: function (scope, element, attr) {
            var msg = attr.confirmClick || "Are you sure?";
            element.bind('click', function () {
                scope.confirmClickModalCtrl.open(msg, attr.ngClick, scope);
            });

            scope.confirmClickModalCtrl = $rootScope.confirmClickModalCtrl;
        }
    };
}]);