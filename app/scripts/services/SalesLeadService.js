/**
 * Created by manojp on 16-10-2014.
 */

'use strict';

angular.module('SalesTrackerApp').factory('salesLeadService', [ '$q', '$resource', 'BASE_URL','$cookieStore',
    function ( $q, $resource, BASE_URL,$cookieStore) {
        return {
            getLoggedUser: function(){
                return $cookieStore.get("logged-user");
            },
            getUserByKey: function(loginKey){
                var deferred = $q.defer();
                $resource(BASE_URL + '/users/getByLoginKey/:loginKey', {loginKey: loginKey},
                    {get:{method:'GET'}})
                    .get({loginKey: loginKey},
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            getUsers: function () {
                var deferred = $q.defer();
                $resource(BASE_URL + '/users/get', {},
                    {get:{method:'GET'}})
                    .get({},
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            getRegions: function () {
                var deferred = $q.defer();
                $resource(BASE_URL + '/regions/get', {},
                    {get:{method:'GET'}})
                    .get({},
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            getIndustries: function () {
                var deferred = $q.defer();
                $resource(BASE_URL + '/industries/get', {},
                    {get:{method:'GET'}})
                    .get({},
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            searchClients: function (searchCriteria) {
                var deferred = $q.defer();
                $resource(BASE_URL + '/clients/search?clientname=:clientName', {clientName: searchCriteria},
                    {get:{method:'GET'}})
                    .get({clientName: searchCriteria},
                    function (response) {
                            deferred.resolve(response.data);
                    },
                    function (response) {
                            deferred.reject(response);
                    });
                return deferred.promise;
            },
            searchUsers: function (searchCriteria,loggedUser) {
                var deferred = $q.defer();
                $resource(BASE_URL + '/collaborators/search?username=:userName&loggedinuserid=:loggedUserId', {userName: searchCriteria,loggedUserId:loggedUser.id},
                    {get:{method:'GET'}})
                    .get({userName: searchCriteria,loggedUserId:loggedUser.id},
                    function (response) {
                            deferred.resolve(response.data);
                    },
                    function (response) {
                            deferred.reject(response);
                    });
                return deferred.promise;
            },
            getClientById: function (client) {
                var deferred = $q.defer();
                $resource(BASE_URL + '/clients/get/:clientId', {clientId: client.id},
                    {get:{method:'GET'}})
                    .get({clientId: client.id},
                    function (response) {
                            deferred.resolve(response.data);
                    },
                    function (response) {
                            deferred.reject(response);
                    });
                return deferred.promise;
            },
            getSocializedTargetClient: function (user) {
                var deferred = $q.defer();
                $resource(BASE_URL + '/targetClientInfo/get/:userId?timestamp='+new Date(), {userId: user.id},
                    {get:{method:'GET'}})
                    .get({userId: user.id},
                    function (response) {
                            deferred.resolve(response.data);
                    },
                    function (response) {
                            deferred.reject(response);
                    });
                return deferred.promise;
            },
            getSocializedProspectClient: function (user) {
                var deferred = $q.defer();
                $resource(BASE_URL + '/targetProspectInfo/get/:userId?timestamp='+new Date(), {userId: user.id},
                    {get:{method:'GET'}})
                    .get({userId: user.id},
                    function (response) {
                            deferred.resolve(response.data);
                    },
                    function (response) {
                            deferred.reject(response);
                    });
                return deferred.promise;
            },
            submitTargetClient: function(targetClientDataToSocialize){
                var deferred = $q.defer();
                var headers = {
                    method: 'POST'
                };
                $resource(BASE_URL + '/targetClient/add', {}, {save: headers})
                    .save({}, targetClientDataToSocialize,
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            sendInvitation: function(userIds,subject,emailBody){
                var deferred = $q.defer();
                var headers = {
                    method: 'POST'
                };
                var request = {
                    userIds:userIds,
                    subject:subject,
                    emailBody:emailBody
                };
                $resource(BASE_URL + '/email/send', {}, {save: headers})
                    .save({}, request,
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            submitProspectClient: function(prospectClientDataToSocialize){
                var deferred = $q.defer();
                var headers = {
                    method: 'POST'
                };
                $resource(BASE_URL + '/targetProspect/add', {}, {save: headers})
                    .save({}, prospectClientDataToSocialize,
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            addCollaborativeUser: function(user,client,isClienTarget){
                var deferred = $q.defer();
                var headers = {
                    method: 'POST'
                };
                var collaborator = {
                    "targetClientId":isClienTarget?client.id:null,
                    "targetProspectId":isClienTarget?null:client.id,
                    "userId":user.id
                };
                $resource(BASE_URL + '/collaborator/add', {}, {save: headers})
                    .save({}, collaborator,
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            deleteCollaborativeUser: function(collaborator){
                var deferred = $q.defer();
                var headers = {
                    method: 'PUT'
                };
                $resource(BASE_URL + '/collaborator/delete/:helperid', {helperid: collaborator.id}, {save: headers})
                    .save({helperid: collaborator.id}, collaborator,
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            addPlanOfAction: function(planOfActionToAdd,client,isClienTarget){
                var deferred = $q.defer();
                var headers = {
                    method: 'POST'
                };
                var planOfAction = {
                    "targetClientId":isClienTarget?client.id:null,
                    "targetProspectId":isClienTarget?null:client.id,
                    "description":planOfActionToAdd
                };
                $resource(BASE_URL + '/actionplan/add', {}, {save: headers})
                    .save({}, planOfAction,
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            updatePlanOfAction: function(planOfActionToUpdate){
                var deferred = $q.defer();
                var headers = {
                    method: 'POST'
                };
                $resource(BASE_URL + '/actionplan/update', {}, {save: headers})
                    .save({}, planOfActionToUpdate,
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            },
            deletePlanOfAction: function(planOfAction){
                var deferred = $q.defer();
                var headers = {
                    method: 'PUT'
                };
                $resource(BASE_URL + '/actionplan/delete/:actionPlanId', {actionPlanId: planOfAction.id}, {save: headers})
                    .save({actionPlanId: planOfAction.id}, planOfAction,
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }
        };
    }]);