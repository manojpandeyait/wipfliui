/**
 * Created by asheesh on 6/3/14.
 */
angular.module('SalesTrackerApp').service('utils', function () {

    return  {
        initRootScope: function ($rootScope, $modal, salesLeadService) {
            $rootScope.loggedUser = salesLeadService.getLoggedUser();
            $rootScope.showUserHeader = function(loggedUser){
                $rootScope.loggedUser = loggedUser;
                $rootScope.isUserHeaderVisible = true;
            };
            $rootScope.hideUserHeader = function(){
                $rootScope.isUserHeaderVisible = false;
            };

            //Loading Indicator
            $rootScope.isLoading = false;
            var loadingCount = 0;
            $rootScope.showLoading = function () {
                $rootScope.isLoading = true;
                loadingCount++;
            };
            $rootScope.hideLoading = function () {
                loadingCount--;
                if (loadingCount <= 0) {
                    $rootScope.isLoading = false;
                }
            };

            //Toastr Util

            $rootScope.showToastrAlerts = function (toastType, title, message, options) {
                toastr.options = {
                    tapToDismiss: true,
                    closeButton: true,
                    positionClass: "toast-top-full-width",
                    onclick: null,
                    onShown: null,
                    onHidden: null,
                    showDuration: 500,
                    hideDuration: 500,
                    timeOut: 0,
                    extendedTimeOut: 0,
                    showEasing: 'swing',
                    hideEasing: 'swing',
                    showMethod: 'fadeIn',
                    hideMethod: 'fadeOut',
                    tapToDismiss: true,
                    newestOnTop: true
                };
                if (angular.element("body .modal-backdrop").length > 0) {
                    if (angular.element(".toast-top-full-width").length > 0) {
                        angular.element(".toast-top-full-width").addClass("toast-at-top");
                    }
                    options.positionClass = "toast-top-full-width toast-at-top";
                }

                angular.extend(toastr.options, options);
                return toastr[toastType](message, title);
            };
            $rootScope.showToastrErrors = function (message) {
                var msg = 'Error : ' + message;
                $rootScope.showToastrAlerts("error", "", msg);
            };
            $rootScope.showToastrSuccess = function (msg) {
                var toastOptions = {timeOut: 3000, extendedTimeOut: 3000};
                $rootScope.showToastrAlerts("success", "", msg,toastOptions);
            };
            $rootScope.clearToastrAlerts = function (toasts) {
                angular.forEach(toasts, function (toast) {
                    toastr.clear(toast);
                });
            };

            //numbers formatting
            $rootScope.maskNumbers = function (number, places, prefix, suffix, thousand) {
                if (!number) {
                    number = "";
                }
                places = !isNaN(places = Math.abs(places)) ? places : 2;
                thousand = thousand || "";
                if (number == "") {
                    prefix = "";
                    suffix = "";
                }
                var decimalValue = number.toString().split(".")[1];
                var hasDecimal = number.toString().indexOf(".") < 0;
                var integerValue = number.toString().split(".")[0];
                var number = number,
                    i = hasDecimal ? number + "" : integerValue + "",
                    j = (j = i.length) > 3 ? j % 3 : 0,
                    k = (hasDecimal || decimalValue.length == 0 || parseInt(decimalValue) == 0) ? "" : "." + decimalValue;

                return prefix + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + k + suffix;
            };

            //confirm Click Directive Helper
            $rootScope.confirmClickModalCtrl = {
                controller: function ($scope, $modalInstance, confirmClickMsg, clickAction, clickScope) {
                    $scope.confirmClickModalData = {
                        header: "Confirm"
                    };
                    $scope.confirmClickModalData.confirmClickMsg = confirmClickMsg;
                    $scope.ok = function () {
                        clickScope.$eval(clickAction);
                        $modalInstance.dismiss('ok');
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.$on('closeAllModalPopup', function () {
                        $modalInstance.dismiss('cancel');
                    });
                },
                open: function (msg, clickActionLocal, clickScope) {
                    var _this = this;
                    var modalInstance = $modal.open({
                        templateUrl: 'confirmClickModal.html',
                        controller: _this.controller,
                        keyboard: true,
                        backdrop: 'static',
                        resolve: {
                            confirmClickMsg: function () {
                                return msg;
                            },
                            clickAction: function () {
                                return clickActionLocal;
                            },
                            clickScope: function () {
                                return clickScope;
                            }
                        },
                        windowClass: "spb-confirm-click-modal"
                    });
                }
            };

            //textarea auto expand
            $rootScope.setTextAreaHeight = function (textarea) {
                $(textarea).css("height", (textarea.scrollHeight - 22) + "px");
                var currentHeight = textarea.scrollHeight - 2;
                $(textarea).css("height", currentHeight + "px");
            };
            $(document)
                .on('input.textarea', '.auto-expand', function () {
                    $rootScope.setTextAreaHeight(this);
                });
        }
    };
});

angular.module('SalesTrackerApp').run(['$rootScope', '$modal', 'utils','salesLeadService',
    function ($rootScope, $modal, utils,salesLeadService) {
        utils.initRootScope($rootScope, $modal,salesLeadService);
    }]);

