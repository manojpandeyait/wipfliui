/**
 * Created by manojp on 16-10-2014.
 */
angular.module('SalesTrackerApp')
    .constant('BASE_URL', '/summit')
    .constant('SEARCH_TIMEOUT', 2000);