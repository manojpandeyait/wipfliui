(function() {
    'use strict';

/**
 * @ngdoc config
 * @name $routeProvider
 *
 * # $routeProvider
 *
 * @description
 * routes configuration across the board 
 */

    angular.module('SalesTrackerApp')
        .config(['$routeProvider', '$locationProvider', routeConfig]);

    function routeConfig($routeProvider, $locationProvider) {
        $routeProvider
            .when('/login/:loginKey', {
                templateUrl: './views/login.html'
            })
            .when('/welcome',{
                templateUrl: './views/welcome.html'
            })
            .when('/error/:errorCode', {
                templateUrl: './views/error.html'
            })
            .when('/admin', {
                templateUrl: './views/admin.html'
            })
            .otherwise({
                redirectTo: '/error/404'
            });


//        $locationProvider.html5Mode(true);
    };
})();
